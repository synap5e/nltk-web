nltk-web
========

A quick server thing for accessing python nltk online.

This is not good code, the emphasis was on quickly deploying something to mess around with friends.
The python server leaks memory and very simple sanitation of data is performed meaning there may be security issues (e.g. XSS).

Use at your own risk
