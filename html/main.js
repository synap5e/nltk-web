endpoint = "http://synap5e.info:8080/"

function getURLParameter(name) {
	// from http://stackoverflow.com/a/8764051
    return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null;
}

$(document).ready(function() {
	var id = getURLParameter('id')
	if (id == null){
		$("#similar").hide();
		$("#random").hide();
		$("#value").hide();
		$("#share_prompt").hide();
		$("#go").click(function() {
			$.ajax({
			  type: "POST",
			  url: endpoint,
			  data: "text=" + encodeURI($("#text").val().replace( /[^a-zA-Z\']+/g," ")),
			  success: function(id){
				window.location = "http://nltk.synap5e.info/?id=" + id;
			  },
			});
		});
	} else {
		$("#text_prompt").hide();
		$("#go").hide();
		$("#text").prop("disabled", true);
		$("#random").click(function() {
			$("#share").val("http://nltk.synap5e.info/?id=" + id + "&rlen=" + $("#value").val())
			$.ajax({
			  type: "POST",
			  url: endpoint,
			  data: "id=" + id + "&type=random&word=" + encodeURI($("#value").val()) + "&length=" + encodeURI($("#value").val()),
			  success: function(data){
				$("#text").val(data);
			  },
			});
		});
		$("#similar").click(function() {
			$("#share").val("http://nltk.synap5e.info/?id=" + id + "&word=" + $("#value").val())
			$.ajax({
			  type: "POST",
			  url: endpoint,
			  data: "id=" + id + "&type=similar&word=" + encodeURI($("#value").val()) + "&length=" + encodeURI($("#value").val()),
			  success: function(data){
				$("#text").val(data);
			  },
			});
		});
		
		var rlen = getURLParameter('rlen');
		if (rlen != null){
			$("#value").val(rlen);
			$("#random").click();
		}
		
		var word = getURLParameter('word');
		if (word != null){
			$("#value").val(word);
			$("#similar").click();
		}
	}
});