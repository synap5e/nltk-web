import time, cgi, string, random, BaseHTTPServer, re
from nltk.text import Text
from nltk.util import tokenwrap
from nltk.probability import FreqDist

HOST_NAME = '0.0.0.0'
PORT_NUMBER = 8080

texts = dict()

def id_generator(size=6, chars=string.ascii_uppercase + string.digits + string.lowercase):
	return ''.join(random.choice(chars) for x in range(size))
	
def similar(self, word, num=20):
	word = word.lower()
	wci = self._word_context_index._word_to_contexts 
	if word in wci.conditions(): 
		contexts = set(wci[word]) 
		fd = FreqDist(w for w in wci.conditions() for c in wci[w] if c in contexts and not w == word) 
		words = fd.keys()[:num] 
		return tokenwrap(words) 
	else: 
		return "No matches" 

class MyHandler(BaseHTTPServer.BaseHTTPRequestHandler):
	def do_HEAD(self):
		self.send_response(200)
		self.send_header("Content-type", "text/html")
		self.end_headers()
	def do_GET(self):
		"""Respond to a GET request."""
		self.send_response(200)
		self.send_header("Content-type", "text/html")
		self.end_headers()
		self.wfile.write("<html><head><title>You Came to the Wrong Neighborhood</title></head>")
		self.wfile.write("<body><p>Sorry, this is an api endpoint server and does not support GET</p>")
		self.wfile.write("</body></html>")
		
	def do_POST(self):
		"""Respond to a POST request."""
		ctype = cgi.parse_header(self.headers.getheader('content-type'))[0]
		if not ctype == 'application/x-www-form-urlencoded':
			self.send_response(400)
			return
		postvars = cgi.parse_qs(self.rfile.read(int(self.headers['content-length'])), keep_blank_values=1)
		
		if 'text' in postvars:
			raw_text = postvars['text'][0].split(" ")
			id = id_generator()
			print '>>>\tAdding text of length %d for key "%s"' % (len(raw_text), id)
			text = Text(raw_text)
			text.generate(0) # build the ngram index
			text.similar('hello') # build word-context index
			self.send_response(200)
			texts[id] = text
			self.send_header("Content-type", "text/html")
			self.send_header('Access-Control-Allow-Origin', '*')
			self.end_headers()
			self.wfile.write(id)
			return
		elif 'id' in postvars and postvars['id'][0] in texts and 'type' in postvars:
			type = postvars['type'][0]
			id = postvars['id'][0]
			text = texts[id]
			if type == 'random':
				length = 300
				if 'length' in postvars and postvars['length'][0].isdigit():
					length = max(1, min(1000	, int(postvars['length'][0])))
				print '>>>\tGenerating random text of length %d for sample "%s"' % (length, id)
				self.send_response(200)
				self.send_header("Content-type", "text/html")
				self.send_header('Access-Control-Allow-Origin', '*')
				self.end_headers()
				self.wfile.write(tokenwrap(text._trigram_model.generate(length)))
				return
			if type == 'similar' and 'word' in postvars:
				word = postvars['word'][0]
				print '>>>\tFinding similar words to "%s" for sample "%s"' % (word, id)
				self.send_response(200)
				self.send_header("Content-type", "text/html")
				self.send_header('Access-Control-Allow-Origin', '*')
				self.end_headers()
				self.wfile.write(similar(text, word, 50))
				return
		self.send_response(400)
		self.send_header("Content-type", "text/html")
		self.send_header('Access-Control-Allow-Origin', '*')
		self.end_headers()
		self.wfile.write("Can't process request")

if __name__ == '__main__':
	server_class = BaseHTTPServer.HTTPServer
	httpd = server_class((HOST_NAME, PORT_NUMBER), MyHandler)
	print time.asctime(), "Server Starts - %s:%s" % (HOST_NAME, PORT_NUMBER)
	try:
		httpd.serve_forever()
	except KeyboardInterrupt:
		pass
	httpd.server_close()
	print time.asctime(), "Server Stops - %s:%s" % (HOST_NAME, PORT_NUMBER)